/*
 RMIT University Vietnam
 Course: COSC2659 iOS Development
 Semester: 2017C
 Assignment: 2
 Author: Nguyen Ngoc Hai
 ID: 3577550
 Created date: 11/12/2017
 
 Acknowledgement:
 https://www.youtube.com/watch?v=nPf5X5z0eA4
 https://developer.apple.com/documentation/uikit/uicollectionview/
 https://stackoverflow.com/questions/41162610/create-directory-in-swift-3-0
 https://stackoverflow.com/questions/7638831/fade-dissolve-when-changing-uiimageviews-image
 https://www.youtube.com/watch?v=zZJpsszfTHM
 */

import UIKit
import CoreData

class SingleImageViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    let ENTITY_PHOTO_STR = "EntityMyPhoto"
    let ENTITY_PHOTO_DESC_STR = "desc"
    let ENTITY_PHOTO_IMG_STR = "imageData"
    let ENTITY_ID = "objectID"
    var photos = [Photo]()
    var displayPhotoIndex = 0
    var itemsPerRow:CGFloat = 4
    var context: NSManagedObjectContext? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate?.persistentContainer.viewContext
        imageView.image = photos[displayPhotoIndex].theImage
        textView.text = photos[displayPhotoIndex].description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // change to the left image
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        if displayPhotoIndex < (photos.count - 1){
            displayPhotoIndex += 1
            UIView.transition(
                with: self.imageView,
                duration: 0.5,
                options: UIViewAnimationOptions.transitionFlipFromRight,
                animations: {self.imageView.image = self.photos[self.displayPhotoIndex].theImage},
                completion: nil)
            textView.text = photos[displayPhotoIndex].description
        }
    }
    // change to the right image
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        if displayPhotoIndex > 0 {
            displayPhotoIndex -= 1
            UIView.transition(
                with: self.imageView,
                duration: 0.5,
                options: UIViewAnimationOptions.transitionFlipFromLeft,
                animations: {self.imageView.image = self.photos[self.displayPhotoIndex].theImage},
                completion: nil)
            textView.text = photos[displayPhotoIndex].description
        }
    }
    // popup input field, get the first 140 characters from input field
    @IBAction func editDesc(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Edit Description", message: "maximum length is 140", preferredStyle: .alert)
        alert.addTextField(configurationHandler: {(textView) in
            textView.text = self.photos[self.displayPhotoIndex].description
        })
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {(UIAlertAction) in
            let textField = alert.textFields![0]
            var text = textField.text!
            if text.count > 140 {
               text = text.substring(to: text.index(text.startIndex, offsetBy: 140))
            }
            self.photos[self.displayPhotoIndex].description = text
            self.updateDesc()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     update description of the current displaying photo
     - returns: void
     */
    func updateDesc(){
        let currentPhoto = photos[displayPhotoIndex]
        do {
            context?.object(with: currentPhoto.id).setValue(currentPhoto.description, forKey: ENTITY_PHOTO_DESC_STR)
            try context?.save()
            textView.text = currentPhoto.description
        } catch {
            print("error deleting objec")
        }
    }
    
    // built-in function, sync data between 2 views
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let mainViewController = segue.destination as! ViewController
        mainViewController.itemsPerRow = self.itemsPerRow
    }
}
