/*
 RMIT University Vietnam
 Course: COSC2659 iOS Development
 Semester: 2017C
 Assignment: 2
 Author: Nguyen Ngoc Hai
 ID: 3577550
 Created date: 11/12/2017
 
 Acknowledgement:
 https://www.youtube.com/watch?v=nPf5X5z0eA4
 https://developer.apple.com/documentation/uikit/uicollectionview/
 https://stackoverflow.com/questions/41162610/create-directory-in-swift-3-0
 https://stackoverflow.com/questions/7638831/fade-dissolve-when-changing-uiimageviews-image
 https://www.youtube.com/watch?v=zZJpsszfTHM
 */

import UIKit
import CoreData

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    let ENTITY_PHOTO_STR = "EntityMyPhoto"
    let ENTITY_PHOTO_DESC_STR = "desc"
    let ENTITY_PHOTO_IMG_STR = "imageData"
    let ENTITY_ID = "objectID"
    let INTER_ITEMS_SPACING:CGFloat = 3
    let MAX_ITEMS_PER_ROW:CGFloat = 7
    
    var touchedPhotoIndex:Int = 0
    var itemsPerRow:CGFloat = 4
    var photos = [Photo]()
    var context: NSManagedObjectContext? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate?.persistentContainer.viewContext  // setup context for CoreData activities
        
        // load sample files to the data base if it is empty
        photos = loadDataPhotos(context: context!)
        if (photos.count == 0){
            saveSampleData(context: context!)
            photos = loadDataPhotos(context: context!)
        }
        
        // setup collectionview source and update layout
        collectionView.dataSource = self
        updateLayoutSize()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // COLLECTIONVIEW METHODS
    func collectionView( _ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    /**
    Setup image and gesture recognizer for each cell
     */
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! CollectionViewCell
        cell.imageHolder.image = photos[indexPath.row].theImage
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTap(_:))))
        cell.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(imageHold(_:))))
        return cell
    }
    
    
    /**
     load all photos from CoreData
     - parameter context: context works with CoreData
     - returns: a list of Photos fetched from CoreData
     */
    func loadDataPhotos(context: NSManagedObjectContext) -> [Photo] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_PHOTO_STR)
        request.returnsObjectsAsFaults = false
        var resultPhotos = [Photo]()
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    let desc = result.value(forKey: ENTITY_PHOTO_DESC_STR) as! String
                    let image = UIImage(data: result.value(forKey: ENTITY_PHOTO_IMG_STR) as! Data)
                    let id = result.objectID
                    resultPhotos.append(Photo(id: id, image: image!, description: desc))
                }
                return resultPhotos
            } else {
                return resultPhotos
            }
        } catch {
            print("load data error")
        }
        return resultPhotos
    }
    /**
     Save a Photo object to CoreData
     - parameter photo: a Photo object to be added
     - parameter context: context works with CoreData
     - returns: true if the Photo is added successfully, false otherwise
     */
    func saveDataPhoto(photo: Photo, context: NSManagedObjectContext) -> Bool {
        let newPhoto = NSEntityDescription.insertNewObject(forEntityName: ENTITY_PHOTO_STR, into: context)
        newPhoto.setValue(photo.description, forKey: ENTITY_PHOTO_DESC_STR)
        newPhoto.setValue(UIImagePNGRepresentation(photo.theImage)! as NSData, forKey: ENTITY_PHOTO_IMG_STR)
        do {
            try context.save()
            return true
        } catch {
            print("save data error")
            return false
        }
    }
    /**
     delete a Photo object from CoreData
     - parameter id: id of a Photo object to be deleted
     - parameter context: context works with CoreData
     - returns: true if the Photo is deleted successfully, false otherwise
     */
    func deleteDataPhoto(id: NSManagedObjectID, context: NSManagedObjectContext) -> Bool {
        do {
            context.delete(context.object(with: id))
            try context.save()
            return true
        } catch {
            print("error deleting objec")
            return false
        }
    }
    /**
     loop through images in asset and add them all into CoreData
     - parameter context: context works with CoreData
     - returns: void
     */
    func saveSampleData(context: NSManagedObjectContext) {
        var i = 0
        while true {
            guard let image = UIImage(named: "\(i).jpeq") else {
                break;
            }
            if !saveDataPhoto(photo: Photo(image: image), context: context) {
                print("fail to save sample image \(i) to coreData")
            }
            i+=1
        }
    }
    
    // built-in function, listener for chosen photo, add it to photos list
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let newPhoto = Photo(image: chosenImage)
        if !saveDataPhoto(photo: newPhoto, context: context!) {
            print("fail to save new image to coreData")
            popUpMessage(title: "store error", content: "fail to save new image to coreData")
        } else {
            photos.append(newPhoto)
        }
        dismiss(animated: true, completion: {self.collectionView.reloadData()})
    }
    
    // built-in function, sync data between 2 views
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let singleImageViewController = segue.destination as! SingleImageViewController
        singleImageViewController.photos = self.photos
        singleImageViewController.displayPhotoIndex = touchedPhotoIndex
        singleImageViewController.itemsPerRow = self.itemsPerRow
    }
    
    /**
     response to user's touches, get current image index and move to SingleImage view
     - parameter sender: touched element
     - returns: void
     */
    func imageTap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.collectionView)
        let index = self.collectionView.indexPathForItem(at: location)![1]
        print("touched on \(String(describing: index))")
        touchedPhotoIndex = index
        performSegue(withIdentifier: "sequeToSingleImage", sender: self)
    }
    /**
     response to user's longPress, get current image index and confirm Deletion
     - parameter sender: touched element
     - returns: void
     */
    func imageHold(_ sender: UILongPressGestureRecognizer) {
        let location = sender.location(in: self.collectionView)
        var indexPath = self.collectionView.indexPathForItem(at: location)
        if indexPath == nil {return}
        let index = indexPath![1]
        print("hold on \(index)")
        let photo = photos[index]
        let alert = UIAlertController(title: "Confirm Deletion", message: "Are you sure to delete this image?", preferredStyle: .actionSheet)
        let actionDelete = UIAlertAction(title: "Sure", style: .destructive, handler: {(UIAlertAction) in
            if self.deleteDataPhoto(id: photo.id, context: self.context!) {
                self.photos.remove(at: self.photos.index(where: { $0.id == photo.id })!)
                self.collectionView.reloadData()
            }
        })
        let actionCancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(actionDelete)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     update collectionView layout according to items per row
     - returns: void
     */
    func updateLayoutSize() {
        let screenWidth = UIScreen.main.bounds.width - INTER_ITEMS_SPACING * 4
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(0, INTER_ITEMS_SPACING*2, 0, INTER_ITEMS_SPACING*2)
        layout.itemSize = CGSize(width: screenWidth/itemsPerRow - INTER_ITEMS_SPACING, height: screenWidth/itemsPerRow - INTER_ITEMS_SPACING)
        layout.minimumLineSpacing = INTER_ITEMS_SPACING*2
        layout.minimumInteritemSpacing = INTER_ITEMS_SPACING
        
        collectionView.collectionViewLayout = layout
    }
    
    // UI interactions
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func makeImageSmaller(_ sender: UIButton) {
        if itemsPerRow < MAX_ITEMS_PER_ROW {
            itemsPerRow += 1
        }
        updateLayoutSize()
        self.collectionView.reloadData()
    }
    @IBAction func makeImageBigger(_ sender: UIButton) {
        if itemsPerRow > 1 {
            itemsPerRow -= 1
        }
        updateLayoutSize()
        collectionView.reloadData()
    }
    @IBAction func addImage(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: "Where to pick image?", preferredStyle: .actionSheet)
        let actionOpenGallery = UIAlertAction(title: "Gallery", style: .default, handler: {(UIAlertAction) in
            self.openGallery()
        })
        let actionOpenCamera = UIAlertAction(title: "Camera", style: .default, handler: {(UIAlertAction) in
            self.openCamera()
        })
        alert.addAction(actionOpenCamera)
        alert.addAction(actionOpenGallery)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    /**
     popup alert message to user
     - parameter sender: touched element
     - returns: void
     */
    func popUpMessage(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
}

