/*
 RMIT University Vietnam
 Course: COSC2659 iOS Development
 Semester: 2017C
 Assignment: 2
 Author: Nguyen Ngoc Hai
 ID: 3577550
 Created date: 11/12/2017
 
 Acknowledgement:
 https://www.youtube.com/watch?v=nPf5X5z0eA4
 https://developer.apple.com/documentation/uikit/uicollectionview/
 https://stackoverflow.com/questions/41162610/create-directory-in-swift-3-0
 https://stackoverflow.com/questions/7638831/fade-dissolve-when-changing-uiimageviews-image
 https://www.youtube.com/watch?v=zZJpsszfTHM
 */

import Foundation
import UIKit
import CoreData

class Photo {
    let theImage: UIImage
    var description: String
    var id = NSManagedObjectID()
    init(image:UIImage, description: String) {
        self.theImage = image
        self.description = description
    }
    init(id: NSManagedObjectID, image:UIImage, description: String) {
        self.id = id
        self.theImage = image
        self.description = description
    }
    init(image: UIImage) {
        self.theImage = image
        self.description = "empty"
    }
}
